MOD_NAME=tcp_probe_rdb
FLUSH=10
PORT=0
FULL=1

ifneq ($(KERNELRELEASE),)
   obj-m := $(MOD_NAME).o
else

KERNELDIR ?= /lib/modules/$(shell uname -r)/build

PWD := $(shell pwd)

default:
	$(MAKE) -C $(KERNELDIR) M=$(PWD) modules
endif

reload: unload load

unload:
	- rmmod tcp_probe_rdb.ko

load:
	insmod tcp_probe_rdb.ko port=${PORT} full=${FULL} flush=${FLUSH}

clean:
	$(MAKE) -C $(KERNELDIR) M=$(PWD) clean
